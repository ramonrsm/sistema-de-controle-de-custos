import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Departamentos extends BaseSchema {
  protected tableName = 'departamentos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary()
      table.string('nome').notNullable()
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
